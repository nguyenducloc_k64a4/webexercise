<?php
session_start();
?>

<?php
date_default_timezone_set('Asia/Ho_Chi_Minh');
?>

<!DOCTYPE html>
<html lang='vn'>
<head><meta charset='UTF-8'></head>
<title>Login</title>
<body>


<form style='margin: 20px 50px 0 35px' enctype="multipart/form-data" action="">
    <table style = 'border-collapse:separate; border-spacing:15px 15px;'>
        <tr height = '40px'>
            <td width = '100px' style = 'color: #ffffff; background-color: #00b206; vertical-align: center; text-align: center; padding: 5px 5px'>
                <label>Họ và tên <t style="color: red">*</t></label>
            </td>
            <td width = '500px' > <div style = 'line-height: 32px ;border-color:#ADD8E6'> <?php echo $_SESSION["name"]?> </div> </td>
        </tr>

        <tr height = '40px'>
            <td style = 'color: #ffffff; background-color: #00b206; vertical-align: central;
             text-align: center; padding: 5px 5px'>
                <label>Giới tính <t style="color: red">*</t> </label>
            </td>
            <td> <div style = 'line-height: 32px ;border-color:#ADD8E6'> <?php echo $_SESSION["gender"]?> </div>
            </td>
        </tr>

        <tr height = '40px'>
            <td style = 'color: #ffffff ;background-color: #00b206; vertical-align: central;
             text-align: center; padding: 5px 5px'>
                <label>Ngày sinh <b style="color: red">*</b></label>
            </td>
            <td><div style = 'line-height: 32px ;border-color:#ADD8E6'> <?php echo $_SESSION["day"] . "/" . $_SESSION["month"] ."/". $_SESSION["year"] ?> </div>
            </td>
        </tr>

        <tr height = '40px'>
            <td width = '100px' style = 'color: #ffffff; background-color: #00b206; vertical-align: center; text-align: center; padding: 5px 5px'>
                <label>Địa chỉ <t style="color: red">*</t></label>
            </td>
            <td width = '500px' ><div style = 'line-height: 32px ;border-color:#ADD8E6'> <?php echo $_SESSION["pro"] . " - " . $_SESSION["city"]  ?> </div>
            </td>
        </tr>

        <tr height = '40px'>
            <td width = '100px' style = 'color: #ffffff; background-color: #00b206; vertical-align: center; text-align: center; padding: 5px 5px'>
                <label>Thông tin khác</label>
            </td>
            <td width = '500px' ><div style = 'line-height: 32px ;border-color:#ADD8E6'> <?php echo $_SESSION["info"]?> </div></td>
        </tr>

    </table>
</form>
</fieldset>
</body>
</html>


