<!DOCTYPE html>
<html lang='vn'>
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1" charset='UTF-8'>
    <script>
        var subjectCity = {
            "Hà Nội": [
                "Hoàng Mai",
                "Thanh Trì",
                "Nam Từ Liêm",
                "Hà Đông",
                "Cầu Giấy"
            ],
            "Hồ Chí Minh": [
                "Quận 1",
                "Quận 2",
                "Quận 3",
                "Quận 7",
                "Quận 9"
            ]
        }
        window.onload = function() {
            var citySel = document.getElementById("city");
            var proSel = document.getElementById("pro");
            for (var x in subjectCity) {
                citySel.options[citySel.options.length] = new Option(x, x);
            }
            citySel.onchange = function() {
                proSel.length = 1;
                var y = subjectCity[this.value];
                for (var i = 0; i < y.length; i++) {
                    proSel.options[proSel.options.length] = new Option(y[i], y[i]);
                }
            }
        }
    </script>
</head>
<body>

<?php
session_start();
if ($_SERVER["REQUEST_METHOD"] == "POST") {
    $check = true;
    if (empty(inputHandling($_POST["name"]))) {
        echo "<div style='color: red;'>Hãy nhập tên</div>";
        $check=false;
    }
    if (empty($_POST["gender"])) {
        echo "<div style='color: red;'>Hãy chọn giới tính</div>";
        $check=false;
    }

    if (empty(inputHandling($_POST["year"])) && empty(inputHandling($_POST["month"])) && empty(inputHandling($_POST["day"]))) {
        echo "<div style='color: red;'>Hãy nhập ngày sinh</div>";
        $check=false;
    }

    if (empty(inputHandling($_POST["city"])) && empty(inputHandling($_POST["pro"]))) {
        echo "<div style='color: red;'>Hãy nhập địa chỉ địa chỉ</div>";
        $check=false;
    }
    if($check){
        $_SESSION = $_POST;
        header('Location: regist_student.php');
    }
}

function inputHandling($data) {
    $data = trim($data);
    $data = stripslashes($data);
    return $data;
}

?>

<?php
$gender = array(
    0 => "Nam",
    1 => "Nữ");
?>

<form style='margin: 20px 50px 0 35px' method="post" action="">
    <h1>
        Form đăng ký sinh viên
    </h1>
    <table style = 'border-collapse:separate; border-spacing:15px 15px;'>
        <tr height = '40px'>
            <td width = '100px' style = 'color: #ffffff; background-color: #00b206; vertical-align: center; text-align: center; padding: 5px 5px'>
                <label>Họ và tên </label>
            </td>
            <td width = '500px' ><input type='text' name= "name" style = 'line-height: 32px ;border-color:#ADD8E6'></td>
        </tr>

        <tr height = '40px'>
            <td style = 'color: #ffffff; background-color: #00b206; vertical-align: central;
             text-align: center; padding: 5px 5px'>
                <label>Giới tính </label>
            </td>
            <td> <?php
                for ($i = 0; $i < count($gender); $i++){
                    echo "<input type = 'radio' name='gender' value='$gender[$i]'
                        style = 'line-height: 32px ; border-color:#ADD8E6; color: #0000fa ; background-color: #2E8BC0' > $gender[$i]" ;
                }
                ?>
            </td>
        </tr>

        <tr height = '40px'>
            <td width = '100px' style = 'color: #ffffff; background-color: #00b206; vertical-align: center; text-align: center; padding: 5px 5px'>
                <label>Ngày sinh </label>
            </td>
            <td> Năm <?php
                echo "<select name=year>";
                echo '<option value="" selected="selected"> </option>';
                for($i=0;$i<=25;$i++){
                    $year=date('Y',strtotime("last day of +$i year"));
                    $year = $year - 40;
                    echo "<option name='$year'>$year</option>";
                }
                echo "</select>";
                ?>
                Tháng <?php
                echo "<select name=month>";
                echo '<option value="" selected="selected"> </option>';
                for($i=1;$i<=12;$i++){
                    echo "<option name='$i'>$i</option> ";
                }
                echo "</select>";
                ?>
                Ngày<?php
                echo "<select name=day>";
                echo '<option value="" selected="selected"> </option>';
                for($i=1;$i<=31;$i++){
                    echo "<option name='$i'>$i</option> ";
                }
                echo "</select>";
                ?>
            </td>
        </tr>

        <tr height = '40px'>
            <td width = '100px' style = 'color: #ffffff; background-color: #00b206; vertical-align: center; text-align: center; padding: 5px 5px'>
                <label>Địa chỉ</label>
            </td>
            <td width = '500px' >
                Thành phố: <select name="city" id="city">
                    <option value="" selected="selected"></option>
                </select>
                Quận: <select name="pro" id="pro">
                    <option value="" selected="selected"></option>
                </select>
            </td>
        </tr>

        <tr height = '70px'>
            <td width = '100px' style = 'color: #ffffff; background-color: #00b206; vertical-align: center; text-align: center; padding: 5px 5px'>
                <label>Thông tin khác</label>
            </td>
            <td width = '500px' ><input type='text' name= "info" style = 'line-height: 70px; border-color:#ADD8E6'></td>
        </tr>


    </table>
    <button style='color: #ffffff; background-color: #00b206; border-radius: 10px; border-color: #1a1a1a;
        width: 125px; height: 39px; border-width: 0.2px; margin: 20px 130px; '>Đăng ký</button>
</form>
</fieldset>
</body>
</html>


