<?php
session_start();
?>

<?php
date_default_timezone_set('Asia/Ho_Chi_Minh');
$today = date("YmdHis");
$temp = explode(".", $_SESSION["file"]["fileupload"]["name"]);
$extension = end($temp);
$newName = "";
for ($iter = 0; $iter < count($temp)-1; $iter++ ) {
    $newName = $newName . $temp[$iter];
}
$name = $newName . "_" . $today . "." . $extension;

$image = $name;
$img= "upload/".$image;

?>

<!DOCTYPE html>
<html lang='vn'>
<head><meta charset='UTF-8'></head>
<title>Login</title>
<body>


<form style='margin: 20px 50px 0 35px' enctype="multipart/form-data" action="">
    <table style = 'border-collapse:separate; border-spacing:15px 15px;'>
        <tr height = '40px'>
            <td width = '100px' style = 'color: #ffffff; background-color: #00b206; vertical-align: center; text-align: center; padding: 5px 5px'>
                <label>Họ và tên <t style="color: red">*</t></label>
            </td>
            <td width = '500px' > <div style = 'line-height: 32px ;border-color:#ADD8E6'> <?php echo $_SESSION["name"]?> </div> </td>
        </tr>

        <tr height = '40px'>
            <td style = 'color: #ffffff; background-color: #00b206; vertical-align: central;
             text-align: center; padding: 5px 5px'>
                <label>Giới tính <t style="color: red">*</t> </label>
            </td>
            <td> <div style = 'line-height: 32px ;border-color:#ADD8E6'> <?php echo $_SESSION["gender"]?> </div>
            </td>
        </tr>

        <tr height = '40px'>
            <td style = 'color: #ffffff ;background-color: #00b206; vertical-align: central;
             text-align: center; padding: 5px 5px'>
                <label>Phân Khoa <b style="color: red">*</b></label>
            </td>
            <td><div style = 'line-height: 32px ;border-color:#ADD8E6'> <?php echo $_SESSION["department"]?> </div>
            </td>
        </tr>

        <tr height = '40px'>
            <td width = '100px' style = 'color: #ffffff; background-color: #00b206; vertical-align: center; text-align: center; padding: 5px 5px'>
                <label>Ngày sinh <t style="color: red">*</t></label>
            </td>
            <td width = '500px' ><div style = 'line-height: 32px ;border-color:#ADD8E6'> <?php echo $_SESSION["birthOfDate"]?> </div>
            </td>
        </tr>

        <tr height = '40px'>
            <td width = '100px' style = 'color: #ffffff; background-color: #00b206; vertical-align: center; text-align: center; padding: 5px 5px'>
                <label>Địa chỉ</label>
            </td>
            <td width = '500px' ><div style = 'line-height: 32px ;border-color:#ADD8E6'> <?php echo $_SESSION["address"]?> </div></td>
        </tr>

        <tr height = '40px'>
            <td width = '100px' style = 'color: #ffffff; background-color: #00b206; vertical-align: center; text-align: center; padding: 5px 5px'>
                <label>Hình Ảnh</label>
            </td>
            <td width = '500px' > <div style = 'line-height: 32px ;border-color:#ADD8E6'>
                    <span>
                        <img src=" <?php echo $img?> ">
                    </span>
                </div>
            </td>
        </tr>


    </table>
    <button style='color: #ffffff; background-color: #00b206; border-radius: 10px; border-color: #1a1a1a;
        width: 125px; height: 39px; border-width: 0.2px; margin: 20px 130px; '>Xác nhận</button>
</form>
</fieldset>
</body>
</html>


