<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <style>
        .message{
            color: #bb260d;
            text-align: center;
        }
    </style>
</head>
<body>
<?php
$listResult = array("1", "2", "3","4", "1", "2","3","4","1","2");
$selectionList = array();
for($i = 1 ; $i<=10 ; $i++){
    $key = "answer".strval($i);
    array_push($selectionList,$_COOKIE[$key]);
}
$mark = 0;
for ($i=0; $i < count($selectionList); $i++) {
    if($listResult[$i]==$selectionList[$i]){
        $mark++;
    }
}
echo "<h2 class='message'>Score : $mark/10 </h2>";
if($mark<4){
    echo "<div class='message'>Bạn quá kém, cần ôn tập thêm</div>";
}elseif($mark<=7){
    echo "<div class='message'>Cũng bình thường</div>";
}else{
    echo "<div class='message'>Sắp sửa làm được trợ giảng lớp PHP</div>";
}
?>
</body>
</html>
