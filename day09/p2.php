<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <style>
        * {
            margin: 0;
            padding: 0;
            box-sizing: border-box;
        }
        .main{
            padding-top:20px;
            margin-left: auto;
            margin-right: auto;
            width: 800px;
            height: auto;
            background-color: white;
        }
        .question_1{

        }
        .question{
            font-size: 20px;
            font-weight: bold;
            text-align: center;
            padding-bottom: 10px;
        }
        .list-answer li {
            font-size: 15px;
            padding-left: 50px;
            padding-top: 10px;
            padding-bottom: 10px;
        }
        .cut{
            width: 100%;
            height: 3px;
            background-color: black;
        }
        .btn-next button{
            background-color: #4CAF50; /* Green */
            border: none;
            color: white;
            padding: 16px 32px;
            text-align: center;
            text-decoration: none;
            display: inline-block;
            font-size: 16px;
            margin: 4px 2px;
            transition-duration: 0.4s;
            cursor: pointer;
        }

        .btn-next button:hover {
            background-color: #4CAF50;
            color: white;
        }
    </style>
</head>
<body>
<?php
session_start();

$listQA = array("1","2","3","4");

if ($_SERVER["REQUEST_METHOD"] == "POST") {

    if(isset($_POST["answer6"])&&isset($_POST["answer7"])&&isset($_POST["answer8"])&&isset($_POST["answer9"])&&isset($_POST["answer10"])){
        $answer6 = $_POST["answer6"];
        $answer7 = $_POST["answer7"];
        $answer8 = $_POST["answer8"];
        $answer9 = $_POST["answer9"];
        $answer10 = $_POST["answer10"];
        setcookie("answer6",$answer6, time() + (86400 * 30), "/");
        setcookie("answer7",$answer7, time() + (86400 * 30), "/");
        setcookie("answer8",$answer8, time() + (86400 * 30), "/");
        setcookie("answer9",$answer9, time() + (86400 * 30), "/");
        setcookie("answer10",$answer10, time() + (86400 * 30), "/");
        header("location: result.php");
    }else{
        echo "<div style='color: red;font-size: 20px;font-weight: bold;text-align: center;'>Hãy nhập trả lời tất cả các câu hỏi</div>";
    }

}

?>
<form name="registerForm" method="post" enctype="multipart/form-data" action="">
    <div class="main">
        <div class="question_6">
            <div class="question"> Cau 6</div>
            <ul class="list-answer" style="list-style-type:none;">
                <?php
                foreach($listQA as $x){
                    echo"<li>
                                            <input class='tick-question' type='radio' value=".$x." name='answer6'>"
                        .$x.
                        "</li>";
                }
                ?>
            </ul>
        </div>
        <div class="question_7">
            <div class="question">Cau 7</div>
            <ul class="list-answer" style="list-style-type:none;">
                <?php
                foreach($listQA as $x){
                    echo"<li>
                                            <input class='tick-question' type='radio' value=".$x." name='answer7'>"
                        .$x.
                        "</li>";
                }
                ?>
            </ul>
        </div>
        <div class="question_8">
            <div class="question">Cau 8</div>
            <ul class="list-answer" style="list-style-type:none;">
                <?php
                foreach($listQA as $x){
                    echo"<li>
                                            <input class='tick-question' type='radio' value=".$x." name='answer8'>"
                        .$x.
                        "</li>";
                }
                ?>
            </ul>
        </div>
        <div class="question_9">
            <div class="question">Cau 9</div>
            <ul class="list-answer" style="list-style-type:none;">
                <?php
                foreach($listQA as $x){
                    echo"<li>
                                            <input class='tick-question' type='radio' value=".$x." name='answer9'>"
                        .$x.
                        "</li>";
                }
                ?>
            </ul>
        </div>
        <div class="question_10">
            <div class="question">Cau 10</div>
            <ul class="list-answer" style="list-style-type:none;">
                <?php
                foreach($listQA as $x){
                    echo"<li>
                                            <input class='tick-question' type='radio' value=".$x." name='answer10'>"
                        .$x.
                        "</li>";
                }
                ?>
            </ul>
        </div>
        <div class="btn-next">
            <button >
                Submit
            </button>
        </div>
    </div>
</form>


</body>
</html>
