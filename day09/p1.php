<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <style>
        * {
            margin: 0;
            padding: 0;
            box-sizing: border-box;
        }
        .main{
            padding-top:20px;
            margin-left: auto;
            margin-right: auto;
            width: 800px;
            height: auto;
            background-color: white;
        }
        .question_1{

        }
        .question{
            font-size: 20px;
            font-weight: bold;
            text-align: center;
            padding-bottom: 10px;
        }
        .list-answer li {
            font-size: 15px;
            padding-left: 50px;
            padding-top: 10px;
            padding-bottom: 10px;
        }
        .btn-next button{
            background-color: #4CAF50; /* Green */
            border: none;
            color: white;
            padding: 16px 32px;
            text-align: center;
            text-decoration: none;
            display: inline-block;
            font-size: 16px;
            margin: 4px 2px;
            transition-duration: 0.4s;
            cursor: pointer;
        }

        .btn-next button:hover {
            background-color: #4CAF50;
            color: white;
        }
    </style>
</head>
<body>
<?php
session_start();

$listQA = array("1","2","3","4");


if ($_SERVER["REQUEST_METHOD"] == "POST") {

    if(isset($_POST["answer1"])&&isset($_POST["answer2"])&&isset($_POST["answer3"])&&isset($_POST["answer4"])&&isset($_POST["answer5"])){
        $answer1 = $_POST["answer1"];
        $answer2 = $_POST["answer2"];
        $answer3 = $_POST["answer3"];
        $answer4 = $_POST["answer4"];
        $answer5 = $_POST["answer5"];
        setcookie("answer1",$answer1, time() + (86400 * 30), "/");
        setcookie("answer2",$answer2, time() + (86400 * 30), "/");
        setcookie("answer3",$answer3, time() + (86400 * 30), "/");
        setcookie("answer4",$answer4, time() + (86400 * 30), "/");
        setcookie("answer5",$answer5, time() + (86400 * 30), "/");
        header("location: p2.php");
    }else{
        echo "<div style='color: red;font-size: 20px;font-weight: bold;text-align: center;'>Hãy nhập trả lời tất cả các câu hỏi</div>";
    }

}

?>
<form name="registerForm" method="post" enctype="multipart/form-data" action="">
    <div class="main">
        <div class="question_1">
            <div class="question">Cau 1</div>
            <ul class="list-answer" style="list-style-type:none;">
                <?php
                foreach($listQA as $x_key){
                    echo"<li>
                                            <input class='tick-question' type='radio' value=".$x_key." name='answer1'>"
                        .$x_key.
                        "</li>";
                }
                ?>
            </ul>
        </div>
        <div class="question_2">
            <div class="question">Cau 2</div>
            <ul class="list-answer" style="list-style-type:none;">
                <?php
                foreach($listQA as $x_key){
                    echo"<li>
                                            <input class='tick-question' type='radio' value=".$x_key." name='answer2'>"
                        .$x_key.
                        "</li>";
                }
                ?>
            </ul>
        </div>
        <div class="question_3">
            <div class="question"> Cau 3</div>
            <ul class="list-answer" style="list-style-type:none;">
                <?php
                foreach($listQA as $x_key){
                    echo"<li>
                                            <input class='tick-question' type='radio' value=".$x_key." name='answer3'>"
                        .$x_key.
                        "</li>";
                }
                ?>
            </ul>
        </div>
        <div class="question_4">
            <div class="question">Cau 4</div>
            <ul class="list-answer" style="list-style-type:none;">
                <?php
                foreach($listQA as $x_key){
                    echo"<li>
                                            <input class='tick-question' type='radio' value=".$x_key." name='answer4'>"
                        .$x_key.
                        "</li>";
                }
                ?>
            </ul>
        </div>
        <div class="question_5">
            <div class="question">Cau 5</div>
            <ul class="list-answer" style="list-style-type:none;">
                <?php
                foreach($listQA as $x_key){
                    echo"<li>
                                            <input class='tick-question' type='radio' value=".$x_key." name='answer5'>"
                        .$x_key.
                        "</li>";
                }
                ?>
            </ul>
        </div>
        <div class="btn-next">
            <button>
                Next
            </button>
        </div>
    </div>
</form>


</body>
</html>
