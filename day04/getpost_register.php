<!DOCTYPE html>
<html lang='vn'> 
<head><meta charset='UTF-8'></head> 
<title>Login</title>
<body>

<?php
$name = $gender = $department = $birthOfDate =  "";

if ($_SERVER["REQUEST_METHOD"] == "POST") {
    if(empty(inputHandling($_POST["name"]))){
        echo "<div style='color: red;'>Hãy nhập tên</div>";
    }
    if (empty($_POST["gender"])) {
        echo "<div style='color: red;'>Hãy chọn giới tính</div>";
    }
    if(empty(inputHandling($_POST["department"]))){
        echo "<div style='color: red;'>Hãy chọn phân khoa</div>";
    }

    if(empty(inputHandling($_POST["birthOfDate"]))){
        echo "<div style='color: red;'>Hãy nhập ngày sinh</div>";
    }
    if (!validateDate($_POST["birthOfDate"])) {
        echo "<div style='color: red;'>Hãy nhập ngày sinh đúng định dạng</div>";
    }

}

function inputHandling($data) {
    $data = trim($data);
    $data = stripslashes($data);
    return $data;
}

function validateDate($date){
    if (preg_match("/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/",$date)) {
        return true;
    } else {
        return false;
    }

}
?>

<?php
$gender = array(
    0 => "Nam",
    1 => "Nữ");

$departments = array("" => "",
    "MAT" => "Khoa học máy tính",
    "KDL" => "Khoa học vật liệu" );
?>

    <form style='margin: 20px 50px 0 35px' method="post" action="">
        <table style = 'border-collapse:separate; border-spacing:15px 15px;'>
        <tr height = '40px'>
            <td width = '100px' style = 'color: #ffffff; background-color: #00b206; vertical-align: center; text-align: center; padding: 5px 5px'>
                <label>Họ và tên <t style="color: red">*</t></label>
            </td>
            <td width = '500px' ><input type='text' name= "name" style = 'line-height: 32px ;border-color:#ADD8E6'></td>
        </tr>
        
        <tr height = '40px'>
            <td style = 'color: #ffffff; background-color: #00b206; vertical-align: central;
             text-align: center; padding: 5px 5px'>
                <label>Giới tính <t style="color: red">*</t> </label>
            </td>
            <td> <?php
                for ($i = 0; $i < count($gender); $i++){
                echo "<input type = 'radio' name='gender' value='$gender[$i]'
                        style = 'line-height: 32px ; border-color:#ADD8E6; color: #0000fa ; background-color: #2E8BC0' > $gender[$i]" ;
                }
                ?>
            </td>
        </tr>
        
        <tr height = '40px'>
            <td style = 'color: #ffffff ;background-color: #00b206; vertical-align: central;
             text-align: center; padding: 5px 5px'>
                <label>Phân Khoa <b style="color: red">*</b></label>
            </td>
            <td><?php
                echo "<select name='department'; style = 'width: 150px ; height: 40px' >";
                foreach ($departments as $department => $label){
                    echo '<option value="'.$department.'">'.$label.'</option>';
                }
                echo '</select>'
                ?>
            </td>
        </tr>

            <tr height = '40px'>
                <td width = '100px' style = 'color: #ffffff; background-color: #00b206; vertical-align: center; text-align: center; padding: 5px 5px'>
                    <label>Ngày sinh <t style="color: red">*</t></label>
                </td>
                <td width = '500px' ><input type='date' name="birthOfDate" data-date="" data-date-format="DD MMMM YYYY" style = 'line-height: 32px; border-color:#ADD8E6'></td>
            </tr>

            <tr height = '40px'>
                <td width = '100px' style = 'color: #ffffff; background-color: #00b206; vertical-align: center; text-align: center; padding: 5px 5px'>
                    <label>Địa chỉ</label>
                </td>
                <td width = '500px' ><input type='text' name="address" style = 'line-height: 32px; border-color:#ADD8E6'></td>
            </tr>


        </table>
        <button style='color: #ffffff; background-color: #00b206; border-radius: 10px; border-color: #1a1a1a;
        width: 125px; height: 39px; border-width: 0.2px; margin: 20px 130px; '>Đăng ký</button>
</form>
</fieldset>
</body>
</html>


